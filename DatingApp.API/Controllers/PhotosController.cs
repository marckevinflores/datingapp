using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DatingApp.API.Data;
using DatingApp.API.Dtos;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DatingApp.API.Controllers
{
    [Authorize]
    [Route("api/users/{userId}/photos")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private readonly IDatingRepository _repo;
        private readonly IMapper _mapper;
        private readonly IOptions<CloudinarySettings> _cloudinaryConfig;
        private Cloudinary _cloudinary;

        public PhotosController(IDatingRepository repo, IMapper mapper, IOptions<CloudinarySettings> cloudinaryConfig)
        {
            _cloudinaryConfig = cloudinaryConfig;
            _mapper = mapper;
            _repo = repo;

            Account acc = new Account(
                _cloudinaryConfig.Value.CloudName,
                _cloudinaryConfig.Value.ApiKey,
                _cloudinaryConfig.Value.ApiSecret
            );
            _cloudinary = new Cloudinary(acc);
        }
        [HttpGet("{id}", Name="GetPhoto")]
        public async Task<IActionResult> GetPhoto(int id){
            var photoFromRepo = await _repo.GetPhoto(id);
            var photo = _mapper.Map<PhotoForReturnDto>(photoFromRepo);
            return Ok(photo);
        }

        [HttpPost]
        public async Task<IActionResult> AddPhotoForUser(int userId, [FromForm] PhotoForCreationDto photoForCreationDto)
        {
             if(userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized(); //check if the user id match to the token

            var userFromRepo = await _repo.GetUser(userId); // get user from the repo
            var file = photoForCreationDto.File; 
            var uploadResult = new ImageUploadResult(); //create a variable for the instance of ImageUploadResult
            if(file.Length > 0){ // check if there is a file 
                using(var stream = file.OpenReadStream()){ // reading the content of the file
                    var uploadParams = new ImageUploadParams(){
                        File = new FileDescription(file.Name, stream),// pass file name to stream
                        Transformation = new Transformation().Width(500).Height(500).Crop("fill").Gravity("face")
                    };
                    uploadResult = _cloudinary.Upload(uploadParams); // upload to the cloudinary then the result will be store to the variable

                }
            }
            photoForCreationDto.Url = uploadResult.Uri.ToString(); // get the url from cloudinary then store to dto
            photoForCreationDto.PublicId = uploadResult.PublicId; 

            var photo = _mapper.Map<Photo>(photoForCreationDto); //mapping the photo to photoForCreationDto into the Photo then stored to the variable

            if(!userFromRepo.Photos.Any(u => u.IsMain)) //check if the user has no main photo
                photo.IsMain = true;

            userFromRepo.Photos.Add(photo);

            
            if(await _repo.SaveAll()){

                var photoToReturn = _mapper.Map<PhotoForReturnDto>(photo); //data from the photo will be store to PhotoForReturnDto
                return CreatedAtRoute("GetPhoto", new {id = photo.Id}, photoToReturn); // will be return the value from the GetPhoto function
            }
            return BadRequest("Could not add the photo");
            
        }

        [HttpPost("{id}/setMain")]
        public async Task<IActionResult> SetMainPhoto(int userId, int id)
        {
             if(userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var user = await _repo.GetUser(userId); //get user data

            if(!user.Photos.Any(p=> p.Id == id))
                return Unauthorized();

            var photoFromRepo = await _repo.GetPhoto(id); // get the select photo using id

            if(photoFromRepo.IsMain)
                return BadRequest("This is already the main photo"); // add validation for Main Photo

            var currentMainPhoto = await _repo.GetMainPhotoForUser(userId); // get the main photo of the user

            currentMainPhoto.IsMain = false; // change to false the main photo of user

            photoFromRepo.IsMain = true; // change to true the new main photo of user

            if(await _repo.SaveAll())
                return NoContent();
            
            return BadRequest("Could not set photo to main");
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhoto(int userId, int id){
             if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var user = await _repo.GetUser(userId);

            if (!user.Photos.Any(p => p.Id == id))
                return Unauthorized();

            var photoFromRepo = await _repo.GetPhoto(id);

            if (photoFromRepo.IsMain)
                return BadRequest("You cannot delete your main photo");

            if (photoFromRepo.PublicId != null)
            {
                var deleteParams = new DeletionParams(photoFromRepo.PublicId);

                var result = _cloudinary.Destroy(deleteParams);

                if (result.Result == "ok")
                {
                    _repo.Delete(photoFromRepo);
                }
            }

            if (photoFromRepo.PublicId == null)
            {
                _repo.Delete(photoFromRepo);
            }

            if (await _repo.SaveAll())
                return Ok();

            return BadRequest("Failed to delete the photo");
        }
    }
}