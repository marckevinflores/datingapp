using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DatingApp.API.Data;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace DatingApp.API.Helpers
{
    public class LogUserActivity : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
           var resultContext = await next(); //when the action is completed

           var userId = int.Parse(resultContext.HttpContext.User
                .FindFirst(ClaimTypes.NameIdentifier).Value); //getting the id from session claims

            var repo = resultContext.HttpContext.RequestServices.GetService<IDatingRepository>(); // get the repository
            var user = await repo.GetUser(userId); // get the user data using the userid from claims
            user.LastActive = DateTime.Now; // set the lastActive to date now
            await repo.SaveAll(); //save changes
        }
    }
}